#include "ekorren.h"
#include <iostream>
#include <unistd.h>

using namespace std;
using namespace Ekorren;

int main(int argc, char** argv)
{
    ServerInfo info;

    info.server_address = "chat.freenode.net";
    info.port = 6665;
    info.nick = "testekorren";
    info.username = "testekorren";
    info.real_name = "testekorren";
    info.password = "";
    info.channel_name = "#testekorren";
    info.channel_password = "";

    Ekorren::Ekorren();

    // If we have any argument we start reading on standard input
    // and import any data dumped there.
    if (argc == 2)
    {
        string buff;

        while (getline(cin, buff))
        {
            Ekorren::handle_data(buff, "nada", true);
        }

        return 0;
    }

    if (connect(info))
    {
        cout << "Connection error!" << endl;
        return 1;
    }
}
