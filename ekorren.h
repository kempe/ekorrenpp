#ifndef EKORREN_H
#define EKORREN_H

#include <libircclient.h>
#include <libirc_rfcnumeric.h>
#include <string>
#include <sqlite3.h>
#include <vector>
#include <map>
#include <random>

#define DB_NAME "/.ekorren.db"

namespace Ekorren
{
    using data_map = 
    std::map<std::pair<std::string, std::string>, std::vector<std::string> >;

    // IRC handling
    struct ServerInfo
    {
        std::string server_address;
        unsigned short port;

        std::string nick;
        std::string username;
        std::string password;
        std::string real_name;

        std::string channel_name;
        std::string channel_password;
    } static server_info;


    void Ekorren();

    bool connect(ServerInfo info);

    bool join_channel(std::string channel, std::string password);
    bool part_channel(std::string channel);

    bool send_msg_to_channel(std::string channel, std::string message);

    bool is_connected();

    void event_connect(irc_session_t* session, 
        const char* event, const char* origin, const char** params, 
        unsigned int count);

    void event_numeric(irc_session_t* session, 
        unsigned int event, const char* origin, const char** params, 
        unsigned int count);

    void event_channel(irc_session_t* session, 
        const char* event, const char* origin, const char** params, 
        unsigned int count);

    static irc_session_t* session;
    static bool connected = false;

    // Database handling
    static sqlite3* db;

    int db_callback(void* user_data, int column_count, char** data, char** column_names);
    void db_exec(char* sql);
    void create_database();

    std::string get_reverse_db_suffix(bool reverse);
    void insert_data(const data_map& data, bool reverse); 

    struct TupleId 
    {
        size_t id;
        bool success;
    };
    TupleId get_tuple_id(const std::pair<std::string, std::string>& pair, bool reverse);

    std::string get_random_word_with_tuple_id(size_t tuple_id, bool reverse);

    // Get a random tuple containing the given word.
    std::pair<std::string, std::string> get_random_tuple_for_word(const std::string& word);

    std::pair<std::string, std::string> get_random_tuple();


    // Text parsing
    void clean_string(std::string& str, const std::string& allowed = 
            "abcdefghijklmnopqrstuvwxyzåäö ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ!?_-~/.,:");

    data_map process_tokens(const std::vector<std::string>& tokens);
    data_map process_tokens_rev(const std::vector<std::string>& tokens);

    void fix_capitalisation(std::string& talk);

    // Brain
    void handle_data(std::string data, const char* origin, bool disable_network = false);
    std::string speak(const std::pair<std::string, std::string>& start_pair);

    static std::random_device rd;
    static std::mt19937_64 gen(rd());

};

#endif
