#include "ekorren.h"
#include <cstring> // For memset
#include <stdexcept>
#include <thread>
#include <iostream>
#include <algorithm>
#include <locale>
#include <sstream>
#include <iterator>
#include <random>
#include <cstdlib>

using namespace std;

void Ekorren::Ekorren()
{
    irc_callbacks_t callbacks;
    memset(&callbacks, 0, sizeof(callbacks));

    callbacks.event_connect = event_connect;
    callbacks.event_numeric = event_numeric;
    callbacks.event_channel = event_channel;

    session = irc_create_session(&callbacks);

    if (!session)
        throw bad_alloc();

    char* home_dir = getenv("HOME");

    if (!home_dir)
    {
        cerr << "Please set the HOME environment variable to your home directory!" << endl;
        throw exception();
    }

    string path(home_dir);
    path.append(DB_NAME);
    int rc = sqlite3_open(path.c_str(), &db);
    if (rc)
    {
        cerr << "Could not open the database: " << sqlite3_errmsg(db) << endl;
        sqlite3_close(db);
        throw exception();
    }

    create_database();
}

bool Ekorren::connect(ServerInfo info)
{
    cout << "Connecting to " << info.server_address << ":" << info.port << endl
        << "Username: " << info.nick << ", Real name: " << info.real_name
        << ", Nick: " << info.nick << endl;
    bool res = irc_connect(session, info.server_address.c_str(), 
           info.port, info.password.c_str(), info.nick.c_str(), 
           info.username.c_str(), info.real_name.c_str());

    if (res)
    {
        cerr << "Error: " << irc_strerror(irc_errno(session)) << endl;
    }
    else
    {
        cout << "IRC networking thread started!" << endl;
    }

    server_info = info;

    irc_run(session);

    return res;
}

bool Ekorren::join_channel(string channel, string password)
{
    bool res = irc_cmd_join(session, channel.c_str(), password.c_str());

    if (res)
        cerr << "Error: " << irc_strerror(irc_errno(session)) << endl;
    else
        cout << "Joined channel " << channel << "." << endl;

    return res;
}

bool Ekorren::part_channel(string channel)
{
    bool res = irc_cmd_part(session, channel.c_str());

    if (res)
        cerr << "Error: " << irc_strerror(irc_errno(session)) << endl;
    else
        cout << "Left channel " << channel << "." << endl;

    return res;
}

bool Ekorren::send_msg_to_channel(string channel, string message)
{
    bool res = irc_cmd_msg(session, channel.c_str(), message.c_str());

    if (res)
        cerr << "Error: " << irc_strerror(irc_errno(session)) << endl;
    else
        cout << "Sent message \"" << message << "\" to channel " << 
            channel << "." << endl;

    return res;
}

bool Ekorren::is_connected()
{
    return connected;
}

void Ekorren::event_connect(irc_session_t* session, const char* event, 
        const char* origin, const char** params, unsigned int count)
{
    cout << "Connected to the IRC server!" << endl;
    connected = true;

    join_channel(server_info.channel_name, server_info.channel_password);
}

void Ekorren::event_numeric(irc_session_t* session, unsigned int event, 
        const char* origin, const char** params, unsigned int count)
{
}

void Ekorren::event_channel(irc_session_t* session, const char* event, 
        const char* origin, const char** params, unsigned int count)
{
    if (count > 1)
    {
        cout << params[0] << "-" << origin << ": " << params[1] << endl;

        // Handle input data
        handle_data(params[1], origin);
    }
}

int Ekorren::db_callback(void* user_data, int column_count, char** data, char** column_names)
{
    return 0;
}

void Ekorren::db_exec(char* sql)
{
    char* errorMsg = nullptr;
    int rc = sqlite3_exec(db, sql, &db_callback, nullptr, &errorMsg);
    if (rc)
    {
        cerr << "Could not execute the sqlite statement: " << sqlite3_errmsg(db) << endl;
        sqlite3_close(db);
        throw exception();
    }
}

string Ekorren::get_reverse_db_suffix(bool reverse)
{
    return reverse ? "_rev " : " ";
}

void Ekorren::insert_data(const data_map& data, bool reverse)
{
    // Insert the pairs into the database
    sqlite3_stmt* stmt = nullptr;
    sqlite3_prepare_v2(db, ("SELECT rowid FROM word_tuple" + 
                       get_reverse_db_suffix(reverse) + 
                       "WHERE word1 = ?001 AND word2 = ?002").c_str(), -1, &stmt, 0);
    

    sqlite3_stmt* stmt1 = nullptr;
    sqlite3_prepare_v2(db, ("INSERT INTO word_tuple" +
                       get_reverse_db_suffix(reverse) +
                       "(word1, word2) VALUES (?001, ?002)").c_str(), -1, &stmt1, 0);

    if (stmt && stmt1)
    {
        for (auto entry : data)
        {
            unsigned long pair_index = 0;
            sqlite3_bind_text(stmt, 1, entry.first.first.c_str(), -1, SQLITE_STATIC);
            sqlite3_bind_text(stmt, 2, entry.first.second.c_str(), -1, SQLITE_STATIC);

            int res = sqlite3_step(stmt);

            // If the pair is not in the database we insert it
            if (res != SQLITE_ROW)
            {
                sqlite3_bind_text(stmt1, 1, entry.first.first.c_str(), -1, SQLITE_STATIC);
                sqlite3_bind_text(stmt1, 2, entry.first.second.c_str(), -1, SQLITE_STATIC);

                sqlite3_step(stmt1);


                // Fetch the ID of the newly inserted pair
                sqlite3_reset(stmt);

                int res = sqlite3_step(stmt);

                if (res == SQLITE_ROW)
                    pair_index = sqlite3_column_int64(stmt, 0);

                sqlite3_reset(stmt);
                sqlite3_clear_bindings(stmt);
                sqlite3_reset(stmt1);
                sqlite3_clear_bindings(stmt1);

            }
            else
                pair_index = sqlite3_column_int64(stmt, 0);


            {
                sqlite3_stmt* stmt = nullptr;
                sqlite3_prepare_v2(db, ("INSERT INTO word_reference" +
                                   get_reverse_db_suffix(reverse) +
                                   "(tuple_id, word) VALUES (?001, ?002)").c_str(), -1, &stmt, 0);
                if (stmt)
                {
                    for (auto token : entry.second)
                    {
                        sqlite3_bind_int64(stmt, 1, pair_index);
                        sqlite3_bind_text(stmt, 2, token.c_str(), -1, SQLITE_STATIC);

                        sqlite3_step(stmt);
                        sqlite3_reset(stmt);
                        sqlite3_clear_bindings(stmt);

                    }
                    sqlite3_finalize(stmt);
                }
            }
        }
    }

    if (stmt)
        sqlite3_finalize(stmt);

    if (stmt1)
        sqlite3_finalize(stmt1);
}

Ekorren::TupleId Ekorren::get_tuple_id(const pair<string, string>& pair, bool reverse)
{
    sqlite3_stmt* stmt = nullptr;
    sqlite3_prepare_v2(db, ("SELECT rowid FROM word_tuple" + 
                       get_reverse_db_suffix(reverse) + 
                       "WHERE word1 = ?001 AND word2 = ?002").c_str(), -1, &stmt, 0);

    TupleId id_res = { 0, false };
    if (stmt)
    {
        sqlite3_bind_text(stmt, 1, pair.first.c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_text(stmt, 2, pair.second.c_str(), -1, SQLITE_STATIC);

        int res = sqlite3_step(stmt);

        if (res == SQLITE_ROW)
        {
            id_res.id = sqlite3_column_int64(stmt, 0);
            id_res.success = true;
        }

        sqlite3_finalize(stmt);
    }

    return id_res;
}

string Ekorren::get_random_word_with_tuple_id(size_t tuple_id, bool reverse)
{
    vector<string> words;
    stringstream ss;

    sqlite3_stmt* stmt = nullptr;
    sqlite3_prepare_v2(db, ("SELECT word FROM word_reference" + 
                       get_reverse_db_suffix(reverse) + 
                       "WHERE tuple_id = ?001").c_str(), -1, &stmt, 0);

    if (stmt)
    {
        sqlite3_bind_int64(stmt, 1, tuple_id);


        int res = sqlite3_step(stmt);

        while (res == SQLITE_ROW)
        {
            // If the pair is not in the database we insert it
            ss << sqlite3_column_text(stmt, 0);
            words.push_back(ss.str());

            res = sqlite3_step(stmt);

            ss.str("");
        }

        sqlite3_finalize(stmt);
    }

    if (words.empty())
        return string("");

    uniform_int_distribution<> dis(0, words.size() - 1);
    return words[dis(gen)];
}

pair<string, string> Ekorren::get_random_tuple_for_word(const string& word)
{
    stringstream ss("");
    string buff;

    cout << word << endl;

    sqlite3_stmt* stmt = nullptr;
    sqlite3_prepare_v2(db, "SELECT word1, word2 FROM word_tuple WHERE word1 LIKE ?001" \
           " OR word2 LIKE ?002 ORDER BY RANDOM() LIMIT 1", -1, &stmt, 0);

    if (stmt)
    {
        sqlite3_bind_text(stmt, 1, word.c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_text(stmt, 2, word.c_str(), -1, SQLITE_STATIC);

        int res = sqlite3_step(stmt);

        ss << sqlite3_column_text(stmt, 0);
        buff = ss.str();

        ss.str("");
        ss << sqlite3_column_text(stmt, 1);

        res = sqlite3_step(stmt);

        sqlite3_finalize(stmt);
    }

    return make_pair(buff, ss.str());

}

pair<string, string> Ekorren::get_random_tuple()
{
    stringstream ss("");
    string buff;

    sqlite3_stmt* stmt = nullptr;
    sqlite3_prepare_v2(db, "SELECT word1, word2 FROM word_tuple ORDER BY RANDOM() LIMIT 1", -1, &stmt, 0);

    if (stmt)
    {
        int res = sqlite3_step(stmt);

        ss << sqlite3_column_text(stmt, 0);
        buff = ss.str();

        ss.str("");
        ss << sqlite3_column_text(stmt, 1);

        res = sqlite3_step(stmt);

        sqlite3_finalize(stmt);
    }

    return make_pair(buff, ss.str());
}

void Ekorren::create_database()
{
    {
        char sql[] = "CREATE TABLE IF NOT EXISTS word_tuple" \
                     " (word1 TEXT, word2 TEXT, PRIMARY KEY(word1, word2))";
        db_exec(sql);
    }

    {
        char sql[] = "CREATE TABLE IF NOT EXISTS word_reference (tuple_id, word);" \
                     "CREATE INDEX IF NOT EXISTS idx ON word_reference(tuple_id)";
        db_exec(sql);
    }

    {
        char sql[] = "CREATE TABLE IF NOT EXISTS word_tuple_rev" \
                     " (word1 TEXT, word2 TEXT, PRIMARY KEY(word1, word2))";
        db_exec(sql);
    }
    
    {
        char sql[] = "CREATE TABLE IF NOT EXISTS word_reference_rev (tuple_id, word);" \
                     "CREATE INDEX IF NOT EXISTS idx_rev ON word_reference_rev(tuple_id)";
        db_exec(sql);
    }
}

void Ekorren::clean_string(string& str, const string& allowed)
{
    locale loc("sv_SE.utf8");

    auto filter = [&allowed](wchar_t token) 
    { 
        return find(allowed.begin(), allowed.end(), token) == allowed.end();
    };

    str.erase(remove_if(str.begin(), str.end(), filter), str.end());


    transform(str.begin(), str.end(), str.begin(), 
            bind(tolower<wchar_t>, placeholders::_1, loc));
}

Ekorren::data_map Ekorren::process_tokens(const vector<string>& tokens)
{
    map<pair<string, string>, vector<string> > 
        result;

    string prev_token = tokens.front();
    for (size_t i = 1; i < tokens.size() - 1; ++i)
    {   
        result[make_pair(prev_token, tokens[i])].push_back(tokens[i + 1]);
        prev_token = tokens[i];
    }

    return result;
}

Ekorren::data_map Ekorren::process_tokens_rev(const vector<string>& tokens)
{
    map<pair<string, string>, vector<string> > 
        result;

    string prev_token = tokens.back();
    for (size_t i = tokens.size() - 2; i >= 1; --i)
    {   
        result[make_pair(tokens[i], prev_token)].push_back(tokens[i - 1]);
        prev_token = tokens[i];
    }

    return result;
}

void Ekorren::fix_capitalisation(string& talk)
{
    setlocale(LC_ALL, "en_US.UTF-8");

    talk[0] = toupper(talk[0]);

    if (talk.length() < 2)
        return;
    
    for (int i = 2; i < talk.length(); ++i)
    {
        if ((talk[i - 2] == '.' || talk[i - 2] == '!' || 
            talk[i - 2] == '?') && talk[i - 1] == ' ')
        {
            talk[i] = toupper(talk[i]);
        }
    }
}
void Ekorren::handle_data(string data, const char* origin, bool disable_network)
{
    clean_string(data);

    bool address = false;
    if (data.find(server_info.nick, 0) == 0)
        address = true;

    bool respond = false;
    if (data.find(server_info.nick) != string::npos)
        respond = true;

    // Remove the addressing when addressing ekorren directly.
    string identifier = server_info.nick;
    identifier.append(":");
    auto pos = data.find(identifier);
    if (pos != string::npos)
        data.erase(pos, identifier.length());

    // Split the data into tokens
    vector<string> tokens;
    
    istringstream is(data);
    copy(istream_iterator<string>(is),
         istream_iterator<string>(),
         back_inserter(tokens));


    // If we still have no data there isn't much we can do.
    if (tokens.size() == 0)
        return;

    data_map processed_data;
    data_map processed_data_rev;
    if (tokens.size() >= 3)
    {
        // Process the tokens and insert them into the database
        processed_data = process_tokens(tokens);
        processed_data_rev = process_tokens_rev(tokens);
        
        // Insert the new data into the database.
        insert_data(processed_data, false);
        insert_data(processed_data_rev, true);
    }

    if (respond && !disable_network)
    {
        // Pick a random starting word and go from there
        uniform_int_distribution<> length_dis(1, 100);
        string talk;

        unsigned short wanted_length = length_dis(gen);
        pair<string, string> start_pair;

        // Only start with two words if two words exist.
        if (tokens.size() >= 2)
        {
            uniform_int_distribution<> try_dis(0, tokens.size() - 2);
            size_t start_index = try_dis(gen);
            start_pair = 
                make_pair(tokens[start_index], tokens[start_index + 1]);

            talk.append(speak(start_pair));
        }

        // If there was no pair with two words we use a single word as the starting point.
        if (talk == "")//string(start_pair.first + ' ' + start_pair.second))
        {
            uniform_int_distribution<> dis(0, tokens.size() - 1);
            start_pair = get_random_tuple_for_word(tokens[dis(gen)]);

            cout << start_pair.first << " : " << start_pair.second << endl;

            talk.append(speak(start_pair));
        }
        talk.append(" ");

        int counter = 0;
        while (talk.length() < wanted_length)
        {
            pair<string, string> tuple = get_random_tuple();

            string random_speech = speak(tuple);
            if (random_speech == tuple.first.append(" ").append(tuple.second))
            {
                ++counter;

                if (counter > 100)
                    break;

                continue;
            }

            talk.append(random_speech);
            talk.append(" ");
        }

        fix_capitalisation(talk);

        // Ekorren should answer a person directly if that person
        // speaks directly to it.
        if (address)
        {
            char buff[200];
            irc_target_get_nick(origin, buff, 200);

            stringstream ss;

            ss << buff << ": " << talk;

            talk = ss.str();

            address = false;
        }

        // Send the inners thoughts of ekorren via IRC.
        send_msg_to_channel(server_info.channel_name, talk);
    }
}

string Ekorren::speak(const pair<string, string>& start_pair)
{
    string talk;

    TupleId tuple_id = get_tuple_id(start_pair, false);
    if (!tuple_id.success)
        return talk;

    string word = get_random_word_with_tuple_id(tuple_id.id, false);

    talk = start_pair.first + " " + start_pair.second;

    cout << "Start: " << start_pair.first << "," << start_pair.second << endl;

    cout << "####################### FORWARD ##########################" << endl;
    string prev_word = start_pair.second;
    int counter = 0;
    while (word != "" && counter++ < 100)
    {
        talk.append(" ").append(word);

        tuple_id = get_tuple_id(make_pair(prev_word, word), false);

        if (!tuple_id.success)
            break;

        cout << prev_word << "," << word << ": ";

        prev_word = word;

        word = get_random_word_with_tuple_id(tuple_id.id, false);

        cout << word << "(" << tuple_id.id << ")" << endl;

    }

    cout << "###################### REVERSE ###########################" << endl;

    tuple_id = get_tuple_id(start_pair, true);
    word = get_random_word_with_tuple_id(tuple_id.id, true);
    prev_word = start_pair.first;
    counter = 0;
    while (word != "" && counter++ < 100)
    {
       talk.insert(0, word + " "); 

       tuple_id = get_tuple_id(make_pair(word, prev_word), true);

       if (!tuple_id.success)
           break;

       cout << prev_word << "," << word << ": ";

       prev_word = word;

       word = get_random_word_with_tuple_id(tuple_id.id, true);
       cout << word << "(" << tuple_id.id << ")" << endl;
    }

    return talk;
}
